// setup the dependencies
const express = require("express")
const router = express.Router()
// import the controller file
const courseController = require("../controllers/course");
const auth = require("../auth")

// route to get all courses
router.get("/", (req, res) => {
	courseController.getCourses().then(resultFromController => res.send(resultFromController))
})

// route to get specific course

router.get("/:courseId", (req, res) => {
	//  "/:courseId" here is called a "wildcard" and its value is anything that is added at the end of localhost:4000/courses

	// e.g. localhost:4000/courses/dog = the value of our wildcard is "dog"

	// console.log(req.params)

	// req.params always gives an object. The key for thid object is our wildcard, and the value comes from the request endpoint

	courseController.getSpecific(req.params.courseId).then(resultFromController => res.send(resultFromController))
})

// route to create a new course

// when a router sends a specific meyhod to a specific endpoint (in this case a POST request to our /courses endpoint) the code within this route will be run
router.post("/", auth.verify, (req, res) => {
	// auth.verify here is something called "middleware." Middleware is any functgion that must first be resolved before any succeeding code will be executed

	// if your middleware..

	// show in the console the request body
	// console.log(req.body)

	// invoke the addCourse method contained on the courseController module, which is an object. Pages, when imported via Node are treated as objects.

	// We also pass the req.body to addCourse as aprt of the data that it needs

	// Once addCourse has resolved, .then can send whatever it returns (true or false) as its response

	// console.log(auth.decode(req.headers.authorization))

	// res.send("Hello")

	/*
	MINI ACTIVITY (10 Minutes):
	Change the code below to restrict course creation to ONLY admins.

	If a user is not an admin, send a response of false

	*/
	
	if(auth.decode(req.headers.authorization).isAdmin){
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}


})

// route to update a single course
router.put("/:courseId", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin){
		courseController.updateCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}
})


/*
ACTIVITY: 

Create a route and a controller for disabling or archiving course withe the following specifications

1. Route must use a delete request
2. Include middleware to verify user's token
3. Retsrict access to this route to ONLY admin users
4. Name the controller function "archiveCourse"
5. Return true if successful, false is not

HINT: How dow we archive courses?

When finished, copy the course route and course controller files to a2, and push a2 to Gitlab, then paste the link to Boodle
*/

// route to archive a single course
router.delete("/:courseId", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin){
		courseController.archiveCourse(req.params.courseId).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}
})



// export the router
module.exports = router;