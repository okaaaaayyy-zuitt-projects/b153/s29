const Course = require("../models/Course")

module.exports.getCourses = () => {
	// find all courses that are active then put the result in a variable called result and return that result
	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getSpecific = (courseId) => {
	// findById() is like findOne() except it can only find by ID
	return Course.findById(courseId).then(result => {
		return result;
	})
}

module.exports.addCourse = (body) => {

	// console.log(body)

	// create a new object called newCourse based on our Course model. Each of its field's values will come from the request body
	let newCourse = new Course({
		name: body.name,
		description: body.description,
		price: body.price
	})

// use .save() to sabe our newCourse object to our database. If saving is NOT successful, an error message will be contained inside of teh error parameter passed to .then()

// If the error parameter has value, then it will render true in our if statement, and the function will return false

// If saving is successful, the error parameter will be empty, and this render false. This will cause our else statement to be run instead and the function will return true
	return newCourse.save().then((course, error) => {
		if(error){
			return false; 
		}else{
			return true;
		}
	})
}

module.exports.updateCourse = (courseId, body) => {
	let updatedCourse = {
		name: body.name,
		description: body.description,
		price: body.price,
	}

	// use findByIdAndUpdate to find the course we want to update and pass the updatedCourse object as our new course data
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.archiveCourse = (courseId) => {
	let updatedCourse = {
		isActive: false
	}

	// use findByIdAndUpdate to find the course we want to update and pass the updatedCourse object as our ne course data
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}